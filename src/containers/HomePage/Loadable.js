import Loadable from "react-loadable";

const Homepage = Loadable({
  loader: () => import(/* webpackChunkName: "HomePage" */ "./index"),
  loading: () => null,
  modules: ["HomePage"]
});

export default Homepage;
